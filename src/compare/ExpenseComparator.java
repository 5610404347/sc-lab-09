package compare;

import java.util.Comparator;

public class ExpenseComparator implements Comparator<Company>{

	@Override
	public int compare(Company c1, Company c2) {
		double expense1 = c1.getExpense();
		double expense2 = c2.getExpense();
		if (expense1 > expense2) return 1;
		if (expense1 < expense2) return -1;
		return 0;
	}

}
