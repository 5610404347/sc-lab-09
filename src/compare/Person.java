package compare;

public class Person implements Taxable, Comparable<Person>{
	String name;
	double income;
	public Person(String name , double income) {
		this.name = name;
		this.income = income; 

		
	}
	@Override
	public double getTax() {
		double tax = 0;
		if (income <= 300000){
			tax = (income*5)/100;
		}else if(income > 300000){
			tax = income - 300000; 
			tax = (income*10)/100 + 15000;
		}	
		
		return tax;
	}
	@Override
	public int compareTo(Person other) {
		if (this.income < other.income ) { return -1; }
		if (this.income > other.income ) { return 1;  }
		return 0;
	}
	
	public String toString(){
		return "Person name is "+name+" income = "+income;
	}
}
