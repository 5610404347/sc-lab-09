package compare;

public class Company implements Taxable{
	String name;
	double income, expense, profit;
	
	public Company(String name, double income,double expense) {
		this.name = name;
		this.income = income;
		this.expense = expense;
		profit = income - expense;
	}
	
	@Override
	public double getTax() {
		double tax;
		tax = ((income - expense)*30)/100;
		return tax;
	}
	
	public String toString(){
		return "Company name is "+name+" income = "+income+" expense = "+expense+" profit = "+profit;
	}
	
	public double getIncome(){
		return income;
	}

	public double getExpense(){
		return expense;
	}
	
	public double getProfit(){
		return profit;
	}
}
