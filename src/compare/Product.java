package compare;

public class Product implements Taxable, Comparable<Product>{
	String name;
	double price;
	public Product(String name, double price) {
		this.name = name;
		this.price = price;
	}
	
	@Override
	public double getTax() {
		double tax;
		tax = (price*7)/100;
		return tax;
	}
	@Override
	public int compareTo(Product other) {
		if (this.price < other.price) { return -1; }
		if (this.price > other.price) { return 1;  }
		return 0;
	}
	
	public String toString(){
		return "Product name is "+name+" price = "+price;
	}

}
