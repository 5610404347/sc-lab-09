package compare;

import java.util.Comparator;

public class TaxComparator implements Comparator<Taxable>{

	@Override
	public int compare(Taxable t1, Taxable t2) {
		double tax1 = t1.getTax();
		double tax2 = t2.getTax();
		if (tax1 > tax2) return 1;
		if (tax1 < tax2) return -1;
		return 0;
	}
	
}
