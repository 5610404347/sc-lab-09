package compare;

import java.util.ArrayList;

public class TaxCalculator {

	public TaxCalculator() {
	}
	 public double sum(Taxable[] taxList){
		 double sum = 0;
			for (Taxable taxable : taxList) {
				sum += taxable.getTax();
		} 
		return sum;
	 }
}
