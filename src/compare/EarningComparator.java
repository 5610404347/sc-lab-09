package compare;

import java.util.Comparator;

public class EarningComparator implements Comparator<Company>{

	@Override
	public int compare(Company c1, Company c2) {
		double income1 = c1.getIncome();
		double income2 = c2.getIncome();
		if (income1 > income2) return 1;
		if (income1 < income2) return -1;
		return 0;
	}

}
