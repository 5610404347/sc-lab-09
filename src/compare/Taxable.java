package compare;

public interface Taxable {
		public double getTax();
}
