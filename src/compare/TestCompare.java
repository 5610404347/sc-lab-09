package compare;

import java.util.ArrayList;
import java.util.Collections;

public class TestCompare {

	public static void main(String[] args) {
		
		//test case Person
		
		ArrayList<Person> person = new ArrayList<Person>();
		person.add(new Person("Jacob" ,50000));
		person.add(new Person("Harry" ,450000));
		person.add(new Person("Molly" ,390000));
		
		System.out.println("---before sort income in person---");
		for (Person p : person) 
			System.out.println(p);

		Collections.sort(person);
		
		System.out.println("\n---after sort income in person---");
		for (Person p : person) 
			System.out.println(p);
	
		// test case Product
		
		ArrayList<Product> product = new ArrayList<Product>();
		product.add(new Product("Ipad", 23000));
		product.add(new Product("Labtop", 48000));
		product.add(new Product("Purse", 2500));

		System.out.println("\n---before sort price in product---");
		for (Product p : product) 
			System.out.println(p);

		Collections.sort(product);
		
		System.out.println("\n---after sort price in product---");
		for (Product p : product) 
			System.out.println(p);
		
		// test case Company
		
		ArrayList<Company> company = new ArrayList<Company>();
		company.add(new Company("Microsoft", 500000, 350000));
		company.add(new Company("Apple", 990000, 750000));
		company.add(new Company("Facebook", 450000, 200000));
		
		System.out.println("\n---- Before sort Income, Expense, Profit");
		for (Company c : company) {
			System.out.println(c);
		}
		
		Collections.sort(company, new EarningComparator());
		System.out.println("\n---- After sort with Income");
		for (Company c : company) {
			System.out.println(c);
		}
		
		Collections.sort(company, new ExpenseComparator());
		System.out.println("\n---- After sort with Expense");
		for (Company c : company) {
			System.out.println(c);
		}
		
		Collections.sort(company, new ProfitComparator());
		System.out.println("\n---- After sort with Profit");
		for (Company c : company) {
			System.out.println(c);
		}
		
		// test case TaxComparator
		
		ArrayList<Taxable> all = new ArrayList<Taxable>();
		all.add(new Person("Jacob" ,50000));
		all.add(new Product("Ipad", 23000));
		all.add(new Company("Apple", 990000, 750000));
		
		System.out.println("\n---- Before sort Tax");
		for (Taxable t : all) {
			System.out.println(t);
		}
		
		Collections.sort(all, new TaxComparator());
		System.out.println("\n---- After sort with Tax");
		for (Taxable t : all) {
			System.out.println(t);
		}
	}

}
