package compare;

public class TestTax {

	public TestTax() {
		
	}

	public static void main(String[] args) {
		Taxable[] person = new Person[3];
		person[0] = new Person("Jacob" ,50000);
		person[1] = new Person("Harry" ,450000);
		person[2] = new Person("Molly" ,390000);
		
		Taxable[] company = new Company[3];
		company[0] = new Company("Microsoft", 500000, 350000);
		company[1] = new Company("Apple", 990000, 750000);
		company[2] = new Company("Facebook", 180000, 150000);
		
		Taxable[] product = new Product[3];
		product[0] = new Product("Ipad", 23000);
		product[1] = new Product("Labtop", 48000);
		product[2] = new Product("Purse", 2500);
		
		TaxCalculator tc = new TaxCalculator();
		
		System.out.println(tc.sum(person));
		System.out.println(tc.sum(company));
		System.out.println(tc.sum(product));
		
		Taxable[] all = new Taxable[3];
		all[0] = new Person("Jacob" ,5000);
		all[1] = new Company("Microsoft", 5000, 3500);
		all[2] = new Product("Purse", 2500);
		System.out.println(tc.sum(all));
	}

}
