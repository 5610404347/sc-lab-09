package TreeTraversal;

import java.util.ArrayList;

public class PostOrderTravesal implements Traversal{

	@Override
	public ArrayList<Node> traverse(Node node) {
		ArrayList<Node>post = new ArrayList<Node>();
		if (node != null){
			traverse(node.getLeft());
			traverse(node.getRight());
			System.out.print(node.getValue()+" ");
		}
		return post;		
	}

}
