package TreeTraversal;

import java.util.ArrayList;

public class InOrderTraversal implements Traversal{

	@Override
	public ArrayList<Node> traverse(Node node) {
		ArrayList<Node>in = new ArrayList<Node>();
		if (node != null){
			traverse(node.getLeft());
			System.out.print(node.getValue()+" ");
			traverse(node.getRight());
		}
		return in;
	}
}

