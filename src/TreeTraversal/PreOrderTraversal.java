package TreeTraversal;

import java.util.ArrayList;

public class PreOrderTraversal implements Traversal{

	@Override
	public ArrayList<Node> traverse(Node node) {
		ArrayList<Node> pre = new ArrayList<Node>();
		if (node != null){
			System.out.print(node.getValue()+" ");
			traverse(node.getLeft());
			traverse(node.getRight());
		}
		return pre;
	}

}
