package TreeTraversal;

public class TraverseTest {

	public static void main(String[] args) {
		
		Node h = new Node("H",null,null);
		Node e = new Node("E",null,null);
		Node c = new Node("C", null, null);
		Node i = new Node("I",h,null);
		Node d = new Node("D",c,e);
		Node a = new Node("A",null,null);
		Node g = new Node("G",null,i);
		Node b = new Node("B",a,d);
		Node f = new Node("F",b,g);

		ReportConsole report = new ReportConsole();
		PreOrderTraversal pre = new PreOrderTraversal();
		report.display(f, pre);
		InOrderTraversal in = new InOrderTraversal();
		report.display(f, in);
		PostOrderTravesal post = new PostOrderTravesal();
		report.display(f, post);
		
		
	}

}
