package TreeTraversal;

public class ReportConsole {
	public void display(Node root, Traversal travesal){
		
		String[] s = travesal.getClass().toString().split("\\.");
		
		System.out.print("Traverse with "+s[1]+":");
		
		for (Node n : travesal.traverse(root)) {
			System.out.print(n.getValue());
		} 
		System.out.println(" ");
	}
}
